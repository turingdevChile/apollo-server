FROM node:10
WORKDIR /source
COPY /apollo-server-app ./
RUN npm install
EXPOSE 4000
CMD npm run production
