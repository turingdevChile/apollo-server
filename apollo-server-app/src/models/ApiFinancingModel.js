import { RESTDataSource } from "apollo-datasource-rest";
import dotenv from 'dotenv';

/* Get env vars, base urls */
dotenv.config();
const { financingApiBaseUrl } = process.env;

class FinancingAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = financingApiBaseUrl;
  }

  async getKpisFinancing() {
    return this.get(`external_requests/kpi/`, null, {cacheOptions: {ttl: 300}});
  }
}

export default FinancingAPI;
