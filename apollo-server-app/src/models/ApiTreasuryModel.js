import { RESTDataSource } from "apollo-datasource-rest";
import dotenv from 'dotenv';

/* Get env vars, base urls */
dotenv.config();
const { treasuryApiBaseUrl } = process.env;

class TreasuryAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = treasuryApiBaseUrl;
  }

  async getEnterprises() {
    return this.get(`enterprises/`, null, {cacheOptions: {ttl: 300}});
  }
}

export default TreasuryAPI;
