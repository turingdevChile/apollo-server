//import 'regenerator-runtime/runtime';
const resolvers = {
  Query: {
    Kpis: async (_source, _args, { dataSources }) => {
      return {
        financing: dataSources.FinancingAPI.getKpisFinancing(),
        loan: dataSources.LoanAPI.getKpisLoan(),
      }
    },
  },
};

export default resolvers;
