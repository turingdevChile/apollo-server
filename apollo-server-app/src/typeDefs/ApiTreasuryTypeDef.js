import { gql } from "apollo-server";

const typeDef = gql`
  type Enterprise @cacheControl(maxAge: 30) {
    id: Int
    name: String
    document_number: String
  }
  type Enterprises @cacheControl(maxAge: 30) {
    results: [Enterprise]
  }
  extend type Query {
    Enterprises: Enterprises @cacheControl(maxAge: 30)
  }
`;

export default typeDef;
